import { createApp } from "vue";
import { createPinia } from "pinia";
import router from "@/router";
import PrimeVue from "primevue/config";
import App from "./App.vue";
import "primeflex/primeflex.css";
import "primevue/resources/themes/lara-light-teal/theme.css";
import "primeicons/primeicons.css";
import ToastService from 'primevue/toastservice';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const app = createApp(App);
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate)
app.use(pinia);
app.use(ToastService);
app.use(PrimeVue, { ripple: true });
app.use(router);
app.mount("#app");
