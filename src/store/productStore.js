import axios from "axios";
import { defineStore } from "pinia";
import { API_URL } from "@/const";
const useProductStore = defineStore("products", {
  state: () => ({
    products: null,
    categories: null,
    queryFilter: "",
    categoryFilter: null,
  }),
  getters: {
    getFilteredProducts(state) {
      let prds = state.products;
      if (state.queryFilter.length > 2) {
        prds = prds.filter((product) =>
          product.title
            .toLowerCase()
            .includes(state.queryFilter.toLocaleLowerCase())
        );
      }
      if (state.categoryFilter !== null) {
        prds = prds.filter((product) =>
          product.category
            .toLocaleLowerCase()
            .includes(state.categoryFilter.toLocaleLowerCase())
        );
      }
      return prds;
    },
  },
  actions: {
    async fetchProductList() {
      const { data } = await axios.get(`${API_URL}/products`);
      this.products = data;
      return data;
    },
    async fetchCategoryList() {
      const { data } = await axios.get(`${API_URL}/products/categories`);
      this.categories = data;
    },
    setCategory(category) {
      this.categoryFilter = category;
    },
    clearFilter() {
      (this.categoryFilter = null), (this.queryFilter = "");
    },
  },
});

export default useProductStore;
