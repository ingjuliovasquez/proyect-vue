import axios from "axios";
import { API_URL } from "@/const";
import useMainStore from "@/store";
import { defineStore } from "pinia";

const useCartStore = defineStore("cart", {
  state: () => ({
    cartID: null,
    cart: [],
  }),
  actions: {
    async getCart() {
      const { data, status } = await axios.get(`${API_URL}/carts/${cartID}`);
      console.log(data, status);
    },
    async addProduct(product) {
      const userStore = useMainStore();
      const currentCart = this.cart;
      const exist = currentCart.findIndex((item) => item.id === product.id);
      if (exist >= 0) {
        this.cart[exist].quantity++;
      } else {
        product.quantity = 1;
        this.cart.push(product);
      }
      const { data, status } = await axios.post(`${API_URL}/carts`, {
        userId: userStore.userData.userID,
        date: new Date(),
        products: [this.cart],
      });

      return { data, status };
    },
    async updateProduct() {},
    async deleteProduct(id) {
      this.cart = this.cart.filter((item) => item.id !== id);
      return { data: null, status: 200 };
    },
  },
  persist: true,
});

export default useCartStore;
