import axios from "axios";
import { API_URL } from "@/const";
import { defineStore } from "pinia";

const useMainStore = defineStore("main", {
  state: () => ({
    userData: {
      email: "jdiazv@gmail.com",
      username: "julio_divasquez",
      password: "password_10",
      name: {
        firstname: "Julio",
        lastname: "Díaz",
      },
      address: {
        city: "Veracruz",
        street: "7835 new road",
        number: 3,
        zipcode: "12926-3874",
        geolocation: {
          lat: "-37.3159",
          long: "81.1496",
        },
      },
      phone: "1-570-236-7033",
      userID: null,
    },
    showModal: true,
    logged: false,
    userToast: {
      show: false,
      messagge: "",
    },
  }),
  getters: {},
  actions: {
    async createUser() {
      const { data, status } = await axios.post(`${API_URL}/users`);
      if (status === 200 && data.id !== null) {
        this.showModal = false;
        this.logged = true;
        this.userData.userID = data.id;
      }
      return { data, status };
    },
  },
  persist: true,
});

export default useMainStore;
