import { ref } from "vue";

const useNavbar = () => {
  const items = ref([
    {
      label: "Lista de productos",
    },
    {
      label: "Carrito de compras",
    },
  ]);

  return {
    items,
  };
};

export default useNavbar;
