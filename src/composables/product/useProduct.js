import { onMounted, ref, computed, watch } from "vue";
import { storeToRefs } from "pinia";
import useMainStore from "@/store";
import useProductStore from "@/store/productStore";
const useProduct = () => {
  const mainStore = useMainStore();
  const productStore = useProductStore();
  const products = ref(null);

  const modelProduct = (text) => {
    return `${text.slice(0, 30)} ...`;
  };

  const logged = computed(() => mainStore.logged);

  onMounted(() => {
      productStore.fetchProductList();
      productStore.fetchCategoryList();
  });

  return { products, modelProduct };
};

export default useProduct;
