import { onMounted, ref, computed } from "vue";
import useMainStore from "@/store";
const useLayout = () => {
  const mainStore = useMainStore();
  const items = ref([
    {
      label: "Productos",
      icon: "pi pi-home",
      route: "/",
    },
    {
      label: "Carrito",
      icon: "pi pi-shopping-cart",
      route: "/cart",
    },
  ]);


  const showDialog = computed(() => mainStore.showModal);

  onMounted(() => {});

  return {
    items,
    showDialog,
    userData: mainStore.userData,
    createUser: mainStore.createUser,
  };
};

export default useLayout;
