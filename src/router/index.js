import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        component: () => import('@/pages/Layout.vue'),
        children: [
            {
                path: '/',
                component: () => import('@/pages/products/Products.vue')
            },
            
            {
                path: '/cart',
                component: () => import('@/pages/cart/Cart.vue'),
                name: 'cart'
            },

        ]
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router